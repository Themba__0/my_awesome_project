import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:the_good_courier/user_dashboard.dart';
import 'package:the_good_courier/my_drawer_header.dart';
import 'package:page_transition/page_transition.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  static const String _title = "The Good Courier";
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: _title,
      theme: ThemeData(primarySwatch: Colors.indigo),
      home: const SplashScreen(),
    );
  }
}

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
      splash: Column(
        children: [
          Image.asset('images/what_now.jpg'),
          const Text(
            'Milking it',
            style: TextStyle(
                fontSize: 40, fontWeight: FontWeight.bold, color: Colors.grey),
          ),
        ],
      ),
      backgroundColor: Colors.blueAccent,
      nextScreen: MyHomePage(),
      splashIconSize: 250,
      duration: 4000,
      splashTransition: SplashTransition.rotationTransition,
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var currentPage = DrawerSections.dashboard;
  final user = new UserDashboard();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text(
            "The Good Courier",
            //textAlign: ,
          ),
        ),
        drawer: Drawer(
          child: SingleChildScrollView(
            child: Container(
              color: const Color.fromRGBO(50, 75, 205, 0.5),
              child: Column(
                //padding: EdgeInsets.zero,
                children: [
                  const MyDrawerHeader(),
                  MyDrawerList(),
                ],
              ),
            ),
          ),
        ),
        body: Container(
          //child: Icon(Icons.add_box),
          padding: const EdgeInsets.symmetric(horizontal: 15),
          height: MediaQuery.of(context).size.height,
          child: Form(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                TextFormField(
                  decoration: InputDecoration(
                    hintText: "Email",
                    border: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        Radius.circular(15),
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(
                    hintText: "Password",
                    border: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        Radius.circular(15),
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 15,
                ),
                ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const UserDashboard(),
                      ),
                    );
                  },
                  child: const Text("Login"),
                ),
                SizedBox(
                  height: 40,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget MyDrawerList() {
    return Container(
      padding: EdgeInsets.only(top: 15),
      child: Column(
        children: [
          MenuItem(1, "Dashboard", Icons.dashboard_outlined,
              currentPage == DrawerSections.dashboard ? true : false),
          MenuItem(2, "Contacts", Icons.people_alt_outlined,
              currentPage == DrawerSections.contacts ? true : false),
          MenuItem(3, "Events", Icons.event,
              currentPage == DrawerSections.events ? true : false),
          MenuItem(4, "Notes", Icons.notes,
              currentPage == DrawerSections.notes ? true : false),
          MenuItem(5, "Privacy Policy", Icons.privacy_tip_outlined,
              currentPage == DrawerSections.privacyPolicy ? true : false),
          MenuItem(6, "Send Feedback", Icons.feedback_outlined,
              currentPage == DrawerSections.sendFeedback ? true : false),
        ],
      ),
    );
  }

  Widget MenuItem(int id, String title, IconData icon, bool selected) {
    return Material(
      child: InkWell(
        onTap: () {
          Navigator.pop(context);
          setState(() {
            if (id == 1) {
              currentPage == DrawerSections.dashboard;
            } else if (id == 2) {
              currentPage == DrawerSections.contacts;
            } else if (id == 3) {
              currentPage == DrawerSections.events;
            } else if (id == 4) {
              currentPage == DrawerSections.notes;
            } else if (id == 5) {
              currentPage == DrawerSections.privacyPolicy;
            } else if (id == 6) {
              currentPage == DrawerSections.sendFeedback;
            }
          });
        },
        child: Padding(
          padding: EdgeInsets.all(15.0),
          child: Row(
            children: [
              Expanded(
                child: Icon(
                  icon,
                  size: 20,
                  color: Colors.blueGrey,
                ),
              ),
              Expanded(
                flex: 3,
                child: Text(
                  title,
                  style: TextStyle(color: Colors.blueGrey, fontSize: 16),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

enum DrawerSections {
  dashboard,
  contacts,
  events,
  notes,
  privacyPolicy,
  sendFeedback,
}
